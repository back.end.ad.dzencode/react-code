import { API_URLS } from "./constants";
import axios from "axios";

const axiosWrapper = axios.create({});

const API = {
  post: async (data) => {
    const key = await axiosWrapper.post(API_URLS.messages, data);
    return key.data;
  },
  getMessage: async (key) => {
    try {
      const response = await axiosWrapper.get(API_URLS.messages, {
        params: { keyHash: key },
      });
      return response.data;
    } catch (error) {
      return error.message;
    }
  },
  getHash: async (publicKey) => {
    try {
      const response = await axiosWrapper.get(API_URLS.hashes, {
        params: { publicKey: publicKey },
      });
      return response.data.keyHash;
    } catch (error) {
      return error.message;
    }
  },
  getCoupon: async (publicKey) => {
    try {
      const response = await axiosWrapper.get(`${API_URLS.coupon}${publicKey}`);
      return response.data;
    } catch (error) {
      return error.message;
    }
  },
};
export default API;
