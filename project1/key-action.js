export const ADD_TO_KEY = "ADD_TO_KEY";
export const REMOVE_FROM_KEY = "REMOVE_FROM_KEY";
export const GET_KEY = "GET_KEY";
export const SET_PRO = "SET_PRO";
export const GET_PRO = "GET_PRO";
export const CHECK_PRO = "CHECK_PRO";

import { AsyncStorage, Alert } from "react-native";
import API from "../../utils/api";

export const addToKey = (Box) => {
  return { type: ADD_TO_KEY, Box: Box };
};

export const getKey = () => {
  return { type: GET_KEY };
};

export const removeFromKey = (productId) => {
  return { type: REMOVE_FROM_KEY, pid: productId };
};

export const getPro = () => {
  return async (dispatch) => {
    try {
      await AsyncStorage.removeItem("time");
      const enableTime = await addPro();
      dispatch({ type: GET_PRO, expiredTime: enableTime });
    } catch (err) {
      throw new Error(err);
    }
  };
};

export const checkPro = () => {
  return async (dispatch) => {
    try {
      let proAccount = await AsyncStorage.getItem("pro");
      if (proAccount) {
        proAccount = await JSON.parse(proAccount);
      }
      if (!proAccount || new Date() >= new Date(proAccount.expiredTime)) {
        dispatch({
          type: CHECK_PRO,
          payload: {
            isActive: false,
            expiredTime: "",
          },
        });
      } else {
        dispatch({
          type: CHECK_PRO,
          payload: {
            isActive: (proAccount && proAccount.isActive) || false,
            expiredTime: (proAccount && proAccount.expiredTime) || "",
          },
        });
      }
    } catch (err) {
      throw new Error(err);
    }
  };
};

export const setPro = (publicKey) => {
  return async (dispatch) => {
    try {
      const currentTime = new Date().getTime();

      const resData = await API.getCoupon(publicKey);
      const addedTime = +resData + currentTime;
      if (resData) {
        savePro(+resData + currentTime);
        dispatch({ type: SET_PRO, expiredTime: addedTime });
      }
    } catch (err) {
      throw new Error(err);
    }
  };
};

export const setProAccount = (publicKey, isPro) => {
  return async (dispatch) => {
    try {
      const currentTime = new Date().getTime();
      const resData = await API.getCoupon(publicKey);

      if (resData) {
        const addedTime =
          +resData +
          +currentTime +
          (isPro.isActive
            ? new Date(isPro.expiredTime).getTime() - currentTime
            : 0);

        const proAccount = {
          isActive: true,
          expiredTime: new Date(addedTime),
        };

        await AsyncStorage.setItem("pro", JSON.stringify(proAccount));
        dispatch(checkPro());
        return Alert.alert("Pro акаунт активировано");
      } else {
        return Alert.alert("Pro акаунт не активировано");
      }
    } catch (err) {
      return Alert.alert("Pro акаунт не пополнело", err.message);
    }
  };
};

export const savePro = async (data) => {
  let oldStorage = await AsyncStorage.getItem("time");
  oldStorage = JSON.parse(oldStorage);
  oldStorage += data;
  await AsyncStorage.setItem("time", JSON.stringify(oldStorage));
};

export const addPro = async () => {
  const time = await AsyncStorage.getItem("time");
  return time;
};
