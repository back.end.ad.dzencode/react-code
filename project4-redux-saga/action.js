export const LOAD_CURRENT_PAYOUT = "LOAD_CURRENT_PAYOUT";
export const LOAD_FUTURE_PAYOUT = "LOAD_NEXT_PAYOUT";
export const PUT_CURRENT_PAYOUT = "PUT_CURRENT_PAYOUT";
export const PUT_FUTURE_PAYOUT = "PUT_FUTURE_PAYOUT";

export const putCurrentPayout = (dataFromServer) => {
  return {
    type: PUT_CURRENT_PAYOUT,
    nextPayout: dataFromServer,
  };
};

export const putFuturePayout = (dataFromServer) => {
  return {
    type: PUT_FUTURE_PAYOUT,
    futurePayout: dataFromServer,
  };
};

export const getCurrentPayout = () => {
  return {
    type: LOAD_CURRENT_PAYOUT,
  };
};

export const getFuturePayout = () => {
  return {
    type: LOAD_FUTURE_PAYOUT,
  };
};
