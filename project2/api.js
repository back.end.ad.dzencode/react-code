import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

const TOKEN = AsyncStorage.getItem("userToken");

const instance = axios.create({
  baseURL: "https://www.exampleURL/api/",
  headers: {
    "Content-Type": "application/json",
    Authorization: TOKEN,
  },
});
export const mainAppApi = {
  //Auth and get token
  getToken(data) {
    return instance.post("account/login", data, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });
  },
  getDashboardElem() {
    return instance.get("mobile/info");
  },
  getAccountInfo() {
    return instance.get("mobile/account");
  },
  getAddress() {
    return instance.get("mobile/addresses");
  },
  getSupport(pageNumber) {
    return instance.get(`mobile/support?pageSize=25&pageNumber=${pageNumber}`);
  },
  postSupportTicket(data) {
    return instance.post("tickets", data);
  },
  getAllertSettings() {
    return instance.get("alerts/configuration");
  },
  postAlertSetting(data) {
    return instance.post("alerts/configuration", data);
  },
  getAllerts(pageNumber) {
    return instance.get(`mobile/alerts?pageSize=25&pageNumber=${pageNumber}`);
  },
  getAllertsTypes() {
    return instance.get("lookups/alerttypes");
  },
  getRecentlyReports(meterID) {
    return instance.get(`reports/customerReports/${meterID}`);
  },
  downloadRecentlyReports(id) {
    return instance.get(`reports/customerReportPdf/${id}`, {
      headers: {
        "Content-Type": "application/pdf",
      },
    });
  },
  downloadConsumptionReport(meterID, period, fromDate, toDate) {
    //period={daily|monthly}
    return instance.get(
      `meterdata/exportPDF?meterId=${meterID}&period=${period}&fromDate=${fromDate}&toDate=${toDate}`,
      {
        headers: {
          "Content-Type": "application/pdf",
        },
      }
    );
  },
  getMonthlyStatistic(meterID) {
    return instance.get(
      `meterdata/consumption?meterId=${meterID}&period=monthly`
    );
  },
  getElectricityRecentStatistic(meterID) {
    return instance.get(`meterdata/electricity/summary/${meterID}`);
  },
  getWaterOrGazRecentStatistic(meterID) {
    return instance.get(`meterdata/water/summary/${meterID}`);
  },
};
