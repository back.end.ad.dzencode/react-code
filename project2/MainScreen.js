import React, { Component } from "react";
import { View, StyleSheet, FlatList, Text } from "react-native";
import { connect } from "react-redux";
import i18next from "i18next";

import DashboardListItems from "./DashboardListItems";

import { getAlerts } from "../../Store/reducers/alertReducer";
import { getDashboardDetails } from "../../Store/reducers/dashboardReducer";
import { getAccount } from "../../Store/reducers/accountReducer";
import { HeaderIconMenu } from "../../utils/HeaderIcons";
import Loading from "../../utils/Loading";
import getDeviceLang from "../../utils/getDeviceLang";

class MainScreen extends Component {
  goDetails = (id, address) => {
    const { navigation } = this.props;
    navigation.navigate("Details", { id, address });
  };
  componentDidMount() {
    const { getDashboardDetails, getAlerts, getAccount } = this.props;
    getDashboardDetails();
    getAlerts();
    getAccount();
  }

  render() {
    const { meters, getDashboardDetails, isLoading } = this.props;

    if (isLoading) return <Loading />;

    return (
      <View style={styles.container}>
        {meters.length ? (
          <FlatList
            contentContainerStyle={{ paddingBottom: 40 }}
            refreshing={isLoading}
            onRefresh={getDashboardDetails}
            overScrollMode="always"
            style={styles.list}
            data={meters}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <DashboardListItems item={item} goDetails={this.goDetails} /> //каждый елемент списка
            )}
          />
        ) : (
          <Text style={styles.emptyText}>
            {i18next.t("No reports to view")}
          </Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10,
  },
  list: {
    width: "100%",
    paddingTop: 20,
  },
  emptyText: {
    fontSize: 25,
    marginTop: "10%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  headerH: {
    textAlign: "right",
  },
});

MainScreen.navigationOptions = (props) => {
  return {
    headerTitle: i18next.t("dashboard"),
    headerTitleStyle: { marginLeft: 100 },
    headerTitleAlign: "start",
    headerLeft: () => {
      return getDeviceLang(props.screenProps.language) === "Left" ? (
        <HeaderIconMenu navigation={props.navigation} />
      ) : null;
    },
    headerRight: () => {
      return getDeviceLang(props.screenProps.language) !== "Left" ? (
        <HeaderIconMenu navigation={props.navigation} />
      ) : null;
    },
  };
};

const mapStateToProps = (state) => ({
  dashboardElements: state.dashboardReducer.dashboardElements,
  meters: state.dashboardReducer.meters,
  isLoading: state.accountReducer.isLoading,
  language: state.accountReducer.language,
});

export default connect(mapStateToProps, {
  getDashboardDetails,
  getAlerts,
  getAccount,
})(MainScreen);
