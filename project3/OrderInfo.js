import React, { useState, useEffect } from "react";

import { Animated, View, Modal, Text, TouchableOpacity } from "react-native";

import I18n from "../../../../utils/i18n";
import SocketService from "../../../../utils/socketService";
import { humanizeDateDDMMYYYY_HHMMSS } from "../../../../utils/helpers/dateHelpers";
import REQUEST_TYPES from "../../../constants/requestTypes";
import {
  Container,
  Row,
  Col,
  Icon,
  TwoArrowTextInput,
} from "../../../components/common";

import {
  commonStyles,
  typography,
  COLOR_CONSTANT,
  buttonStyles,
} from "../../../stylesheets";

const PRICE_CHANGE_CONSTANTS = { UP: "up", DOWN: "down" };

const InfoRow = ({ label, value, theme, icon, color }) => {
  const IconAndColor = () => {
    let data = {
      icon: "arrow-up",
      color: theme.mainColor,
    };

    if (icon === PRICE_CHANGE_CONSTANTS.UP) {
      data = {
        icon: "arrow-up",
        color: COLOR_CONSTANT.green,
      };
    } else {
      data = {
        icon: "arrow-down",
        color: COLOR_CONSTANT.darkred,
      };
    }

    return data;
  };

  return (
    <Row style={{ minHeight: 24 }}>
      <Col>
        <Text style={[typography.mediumText, { color: theme.secondColor }]}>
          {label}
        </Text>
      </Col>
      <Col>
        <Text
          style={[
            typography.mediumText,
            { color: icon ? IconAndColor().color : theme.mainColor },
          ]}
        >
          {icon && (
            <Icon
              name={IconAndColor().icon}
              size={12}
              color={IconAndColor().color}
            />
          )}
          <Text style={{ color: color || theme.mainColor }}>{value}</Text>
        </Text>
      </Col>
    </Row>
  );
};

const OrderInfo = ({
  data,
  expiredAccount,
  expiredPress,
  visible,
  onClose,
  theme,
  currency,
  orderState,
  symbolDigits = 4,
}) => {
  const {
    ticket,
    system_id,
    symbol,
    type,
    price,
    lot,
    stop_loss,
    take_profit,
    open_time,
    open_price,
    cash,
    swap,
    comment,
    account_id,
  } = data;

  const [stopLoss, setStopLoss] = useState(stop_loss);
  const [takeProfit, setTakeProfit] = useState(take_profit);
  const [prevPrice, setPrevPrice] = useState(0);
  const [arrowIcon, setArrowIcon] = useState(null);

  useEffect(() => {
    if (prevPrice > price) {
      setArrowIcon(PRICE_CHANGE_CONSTANTS.DOWN);
      setPrevPrice(price);
    } else {
      setArrowIcon(PRICE_CHANGE_CONSTANTS.UP);
      setPrevPrice(price);
    }
  }, [price]);

  const handleSubmit = () => {
    SocketService.asyncPost("/terminal/order", {
      account_id,
      order: {
        account_id,
        symbol,
        system_id,
        lot,
        price,
        stop_loss: stopLoss,
        take_profit: takeProfit,
        ticket,
        type,
      },
      type: REQUEST_TYPES.move_order_request,
    });
    onClose();
  };

  const handleCloseOrder = () => {
    if (expiredAccount) {
      expiredPress();
    } else {
      SocketService.asyncPost("/terminal/order", {
        account_id,
        order: {
          account_id,
          symbol,
          system_id,
          lot,
          price,
          stop_loss: stopLoss,
          take_profit: takeProfit,
          ticket,
          type,
        },
        type: REQUEST_TYPES.close_order_request,
      });
      onClose();
    }
  };

  const handleClosePendingOrder = () => {
    SocketService.asyncPost("/terminal/order", {
      account_id,
      order: {
        account_id,
        symbol,
        system_id,
        lot,
        price,
        stop_loss: stopLoss,
        take_profit: takeProfit,
        ticket,
        type,
      },
      type: REQUEST_TYPES.cancel_order_request,
    });
    onClose();
  };

  const changeStopLoss = (valStr) => {
    if (stopLoss === 0) {
      setStopLoss(price);
    } else {
      setStopLoss(valStr);
    }
  };

  const changeTakeProfit = (valStr) => {
    if (takeProfit === 0) {
      setTakeProfit(price);
    } else {
      setTakeProfit(valStr);
    }
  };

  const step = 1 / 10 ** (symbolDigits + 1);

  return (
    <Modal
      animationType="fade"
      transparent={false}
      visible={visible}
      onRequestClose={onClose}
    >
      {visible ? (
        <Animated.ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
          style={{
            width: "100%",
            height: "100%",
            paddingBottom: 24,
            backgroundColor: theme.mainBackground,
          }}
        >
          <Row
            style={{
              paddingHorizontal: 16,
              paddingVertical: 20,
              borderBottomWidth: 1,
              borderBottomColor: theme.secondBackground_01,
            }}
          >
            <TouchableOpacity onPress={onClose}>
              <Icon name="back" size={24} color={theme.mainColor} />
            </TouchableOpacity>
            <Text
              style={[
                typography.h4,
                { color: theme.mainColor, marginStart: 13 },
              ]}
            >
              {symbol}
            </Text>
          </Row>
          <Container>
            <InfoRow label={I18n.t("Ticket")} value={ticket} theme={theme} />
            <InfoRow label={I18n.t("Comment")} value={comment} theme={theme} />
            <InfoRow
              label={I18n.t("Open Date")}
              value={
                open_time
                  ? humanizeDateDDMMYYYY_HHMMSS(new Date(open_time * 1000))
                  : ""
              }
              theme={theme}
            />
            <InfoRow
              label={I18n.t("Position")}
              value={type.replace("_", " ").toUpperCase()}
              theme={theme}
            />
            <InfoRow
              label={I18n.t("Trade Size")}
              value={`${lot} Lot`}
              theme={theme}
            />
            <InfoRow
              label={I18n.t("Open Price")}
              value={open_price || 0}
              theme={theme}
            />
            <InfoRow
              label={I18n.t("Stop Loss")}
              value={stopLoss}
              theme={theme}
            />
            <InfoRow
              label={I18n.t("Take Profit")}
              value={takeProfit}
              theme={theme}
            />
            <InfoRow
              label={I18n.t("Current price")}
              value={price}
              theme={theme}
              icon={arrowIcon}
            />
            {orderState === "opened" && (
              <>
                <InfoRow label={I18n.t("Commission")} value="0" theme={theme} />
                <InfoRow
                  label={I18n.t("SWAP")}
                  value={`${swap} ${currency}`}
                  theme={theme}
                />
                <InfoRow
                  label={I18n.t("Unrealized P/L")}
                  value={`${cash.toFixed(2)} ${currency}`}
                  color={
                    cash.toFixed(2) > 0
                      ? COLOR_CONSTANT.green
                      : COLOR_CONSTANT.darkred
                  }
                  theme={theme}
                />
              </>
            )}
            <View
              style={{
                width: "100%",
                marginTop: 19,
                marginBottom: 8,
                borderBottomWidth: 1,
                borderBottomColor: theme.secondBackground_01,
              }}
            />
            <Text
              style={[commonStyles.mediumLabel, { color: theme.secondColor }]}
            >
              {I18n.t("Stop Loss:")}
            </Text>
            <TwoArrowTextInput
              value={stopLoss}
              onChange={changeStopLoss}
              step={step}
              min={0}
              fixed={symbolDigits + 1}
              theme={theme}
            />
            <Text
              style={[commonStyles.mediumLabel, { color: theme.secondColor }]}
            >
              {I18n.t("Take Profit:")}
            </Text>
            <TwoArrowTextInput
              value={takeProfit}
              onChange={changeTakeProfit}
              min={0}
              max={100}
              fixed={symbolDigits + 3}
              step={step}
              theme={theme}
            />
            <Row style={{ marginTop: 40, marginBottom: 24 }}>
              {orderState === "opened" ? (
                <TouchableOpacity
                  onPress={handleCloseOrder}
                  style={[
                    commonStyles.centeredContent,
                    commonStyles.responsiveFlexItem,
                    buttonStyles.btn,
                    {
                      minHeight: 48,
                      backgroundColor: theme.secondBackground_01,
                    },
                  ]}
                >
                  <Text
                    style={[
                      typography.smallText,
                      { color: theme.seventhColor },
                    ]}
                  >
                    {I18n.t("Close trade").toUpperCase()}
                  </Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={handleClosePendingOrder}
                  style={[
                    commonStyles.centeredContent,
                    commonStyles.responsiveFlexItem,
                    buttonStyles.btn,
                    {
                      minHeight: 48,
                      backgroundColor: theme.secondBackground_01,
                    },
                  ]}
                >
                  <Text
                    style={[
                      typography.smallText,
                      { color: theme.seventhColor },
                    ]}
                  >
                    {I18n.t("Remove order").toUpperCase()}
                  </Text>
                </TouchableOpacity>
              )}

              <TouchableOpacity
                onPress={handleSubmit}
                style={[
                  commonStyles.centeredContent,
                  commonStyles.responsiveFlexItem,
                  buttonStyles.btn,
                  {
                    minHeight: 48,
                    marginStart: 8,
                    backgroundColor: COLOR_CONSTANT.blue,
                  },
                ]}
              >
                <Text
                  style={[
                    typography.smallText,
                    { color: COLOR_CONSTANT.white },
                  ]}
                >
                  {I18n.t("Save changes").toUpperCase()}
                </Text>
              </TouchableOpacity>
            </Row>
          </Container>
        </Animated.ScrollView>
      ) : null}
    </Modal>
  );
};

export default OrderInfo;
