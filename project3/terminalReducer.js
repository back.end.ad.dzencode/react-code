import { handleActions } from "redux-actions";
import RNRestart from "react-native-restart";
import { mergeIn } from "../../utils/helpers/stateHelpers";
import types from "./types";
import SocketService from "../../utils/socketService";

const initialState = {
  user: null,
  error: "",
  pending: false,
  registerData: null,
  registerInfo: null,
  isRegisterContinue: false,
};

const reducer = handleActions(
  {
    [types.GET_USER_SUCCESS]: mergeIn((action) => ({
      user: action.payload.data,
    })),
    [types.USER_LOGOUT_FAIL]: mergeIn(() => {
      SocketService.unsubscribeAll();
      SocketService.disconnect();
      RNRestart.Restart();
      return {
        ...initialState,
      };
    }),
    [types.USER_LOGOUT_SUCCESS]: mergeIn(() => {
      SocketService.unsubscribeAll();
      SocketService.disconnect();
      RNRestart.Restart();
      return {
        ...initialState,
      };
    }),
    [types.USER_LOGIN_SUCCESS]: mergeIn(() => ({
      user: "pending",
      error: null,
    })),
    [types.USER_LOGIN_FAIL]: mergeIn((action) => ({
      user: null,
      error: action.payload || "Error",
    })),
    [types.LOCAL_CHANGE_USER_THEME]: mergeIn((action, state) => ({
      user: { ...state.user, themeDesign: action.payload },
    })),
    [types.LOCAL_CHANGE_USER_LANGUAGE]: mergeIn((action, state) => ({
      user: { ...state.user, default_language_id: action.payload },
    })),
    [types.USER_REGISTER_FAIL]: mergeIn((action) => ({
      registerData: null,
      error: action.payload || "Error",
    })),
    [types.CLEAR_ERROR_MESSAGES]: mergeIn(() => ({ error: "" })),
    [types.USER_REGISTER_SUCCESS]: mergeIn((action) => ({
      registerData: action.payload.data,
      error: null,
    })),
    [types.USER_REGISTRATION_INFO_SUCCESS]: mergeIn((action) => ({
      registerInfo: action.payload.data,
    })),
    [types.USER_REGISTRATION_CONTINUE_SUCCESS]: mergeIn((action, state) => ({
      registerInfo: { ...state.registerInfo, formData: action.payload.data },
      isRegisterContinue: true,
    })),
    [types.USER_UPDATE]: mergeIn(() => ({ pending: true })),
    [types.USER_UPDATE_SUCCESS]: mergeIn((action) => {
      RNRestart.Restart();
      return { user: action.payload.data, pending: false };
    }),
    [types.USER_REGISTER_CHECK_ROLE_SUCCESS]: mergeIn((action) => ({
      registerInfo: action.payload.data,
    })),
    [types.USER_REGISTER_CHECK_ROLE_FAIL]: mergeIn((action) => {
      console.warn("USER_REGISTER_CHECK_ROLE_FAIL", action);
      return {
        isRegisterContinue: false,
      };
    }),
    [types.USER_VALIDATION_FAIL]: mergeIn((action) => {
      console.warn("USER_VALIDATION_FAIL", action.payload);
    }),
    [types.USER_REGISTRATION_CONTINUE_FAIL]: mergeIn((action) => {
      console.warn("USER_REGISTRATION_CONTINUE_FAIL", action.payload);
      return {
        isRegisterContinue: false,
      };
    }),
    [types.USER_UPLOAD_AVATAR_SUCCESS]: mergeIn((action) => ({
      user: action.payload.data,
    })),
  },
  initialState
);

export default reducer;
